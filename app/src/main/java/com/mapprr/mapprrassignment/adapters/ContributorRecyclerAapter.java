package com.mapprr.mapprrassignment.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapprr.mapprrassignment.ContributorDetailsActivity;
import com.mapprr.mapprrassignment.R;
import com.mapprr.mapprrassignment.models.GithubContributor;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ContributorRecyclerAapter extends RecyclerView.Adapter {

    private static final String TAG = ContributorRecyclerAapter.class.getSimpleName();
    private Context context;
    private List<GithubContributor> contributors;
    private LayoutInflater inflater;

    public ContributorRecyclerAapter(Context context, List<GithubContributor> list) {
        this.context = context;
        this.contributors = list;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_contributor_list, parent, false);
        return new ContributorsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            GithubContributor contributor = contributors.get(position);
            ContributorsViewHolder contributorsViewHolder = (ContributorsViewHolder) holder;

            contributorsViewHolder.titleTextView.setText(contributor.getLogin());

            Picasso
                    .with(context)
                    .load(contributor.getAvatar_url())
                    .into(contributorsViewHolder.avatarImageView);
        } catch (Exception e) {
            Log.d(TAG, "Error binding adapter " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return contributors.size();
    }

    private class ContributorsViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        ImageView avatarImageView;

        public ContributorsViewHolder(View view) {
            super(view);

            titleTextView = view.findViewById(R.id.tv_title_item_contributor_list);
            avatarImageView = view.findViewById(R.id.iv_avatar_item_contributor_list);

            view.findViewById(R.id.ll_item_contributor_list).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent contributorDetailsIntent = new Intent(context, ContributorDetailsActivity.class);
                        contributorDetailsIntent.putExtra("contributor", contributors.get(getAdapterPosition()));
                        context.startActivity(contributorDetailsIntent);
                    } catch (Exception e) {
                        Log.d(TAG, "Error clicking contributor " + e.toString());
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
