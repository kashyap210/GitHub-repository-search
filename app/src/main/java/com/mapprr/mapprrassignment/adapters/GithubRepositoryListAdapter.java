package com.mapprr.mapprrassignment.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapprr.mapprrassignment.R;
import com.mapprr.mapprrassignment.RepositoryDetailsActivity;
import com.mapprr.mapprrassignment.models.GithubRepository;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GithubRepositoryListAdapter extends RecyclerView.Adapter {

    private static final String TAG = GithubRepositoryListAdapter.class.getSimpleName();
    private Context context;
    private List<GithubRepository> githubRepositories;
    private LayoutInflater inflater;
    private boolean isScreenLarge;

    public GithubRepositoryListAdapter(Context context, List<GithubRepository> repositories, boolean b) {
        this.context = context;
        this.githubRepositories = repositories;
        this.inflater = LayoutInflater.from(context);
        this.isScreenLarge = !b;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_repo_list, parent, false);
        return new GithubRepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            GithubRepoViewHolder githubRepoViewHolder = (GithubRepoViewHolder) holder;

            GithubRepository repository = githubRepositories.get(position);

            githubRepoViewHolder.titleTextView.setText(repository.getName());
            githubRepoViewHolder.fullNameTextView.setText(repository.getFull_name());
            githubRepoViewHolder.forksCountTextView.setText(String.valueOf(repository.getForks_count()));
            githubRepoViewHolder.starsCountTextView.setText(String.valueOf(repository.getStargazers_count()));
            githubRepoViewHolder.watchersCountTextView.setText(String.valueOf(repository.getWatchers_count()));

            Picasso
                    .with(context)
                    .load(repository.getOwner().getAvatar_url())
                    .into(githubRepoViewHolder.avatarImageView);
        } catch (Exception e) {
            Log.d(TAG, "Error binding the adapter " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return githubRepositories.size();
    }

    public void setData(List<GithubRepository> items) {
        try {
            this.githubRepositories = items;
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.d(TAG, "Error setting Data " + e.toString());
            e.printStackTrace();
        }
    }

    private class GithubRepoViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView, fullNameTextView, starsCountTextView, forksCountTextView, watchersCountTextView;
        ImageView avatarImageView;

        GithubRepoViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.tv_title_item_repo_list);
            fullNameTextView = itemView.findViewById(R.id.tv_full_name_item_repo_list);
            starsCountTextView = itemView.findViewById(R.id.tv_stars_item_list_repo);
            forksCountTextView = itemView.findViewById(R.id.tv_fork_item_repo_list);
            watchersCountTextView = itemView.findViewById(R.id.tv_watchers_count_item_repo_list);
            avatarImageView = itemView.findViewById(R.id.iv_avatar_item_repo_list);

            //Set Image size
            int widthPixels = Resources.getSystem().getDisplayMetrics().widthPixels;
            int newWidth = isScreenLarge ? widthPixels / 10 : widthPixels / 5;

            ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(newWidth, newWidth);
            avatarImageView.setLayoutParams(params);

            itemView.findViewById(R.id.constaint_layout_item_repo_list).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent repoDetailsIntent = new Intent(context, RepositoryDetailsActivity.class);
                    repoDetailsIntent.putExtra("repo_details", githubRepositories.get(getAdapterPosition()));
                    context.startActivity(repoDetailsIntent);
                }
            });
        }
    }
}
