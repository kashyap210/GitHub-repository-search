package com.mapprr.mapprrassignment.extras;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;

import com.mapprr.mapprrassignment.fragments.ProgressBarFragment;

public class CVM {
    public static final String BASE_URL = "https://api.github.com/";
    private static final String TAG = CVM.class.getSimpleName();
    private static final String TAG_PROGRESS_BAR_FRAGMENT = "tag_progress_bar_fragment";

    public static void loadProgressBar(Context context, int id) {
        try {
            ((AppCompatActivity) context)
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(id, new ProgressBarFragment(), TAG_PROGRESS_BAR_FRAGMENT)
                    .commit();
        } catch (Exception e) {
            Log.d(TAG, "Error adding progressbar fragment " + e.toString());
            e.printStackTrace();
        }
    }

    public static void removeProgressBarFragment(Context context, int ll_content_main_activity_main) {
        try {
            Fragment pbFragment = ((AppCompatActivity) context)
                    .getSupportFragmentManager()
                    .findFragmentByTag(TAG_PROGRESS_BAR_FRAGMENT);

            if (pbFragment != null) {
                ((AppCompatActivity) context)
                        .getSupportFragmentManager()
                        .beginTransaction()
                        .remove(pbFragment)
                        .commit();
            }
        } catch (Exception e) {
            Log.d(TAG, "Error removing progressbar fragment " + e.toString());
            e.printStackTrace();
        }
    }
}
