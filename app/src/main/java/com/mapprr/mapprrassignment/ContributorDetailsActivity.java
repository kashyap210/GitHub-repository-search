package com.mapprr.mapprrassignment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapprr.mapprrassignment.adapters.GithubRepositoryListAdapter;
import com.mapprr.mapprrassignment.api.APIAdapter;
import com.mapprr.mapprrassignment.extras.CVM;
import com.mapprr.mapprrassignment.models.GithubContributor;
import com.mapprr.mapprrassignment.models.GithubRepository;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContributorDetailsActivity extends AppCompatActivity {

    private static final String TAG = ContributorDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contributor_details);
        Toolbar toolbar = findViewById(R.id.toolbar);

        try {
            GithubContributor contributor = (GithubContributor) getIntent().getSerializableExtra("contributor");

            toolbar.setTitle(contributor.getLogin());
            setSupportActionBar(toolbar);

            ImageView avatarImageView = findViewById(R.id.iv_avatar_activity_contributor_details);
            Picasso
                    .with(ContributorDetailsActivity.this)
                    .load(contributor.getAvatar_url())
                    .into(avatarImageView);

            //Fetching Repositories
            CVM.loadProgressBar(ContributorDetailsActivity.this, R.id.ll_repositories_content_contributor_details);
            Call<List<GithubRepository>> reposCall = APIAdapter.getApiService().getListByContributor(contributor.getLogin());
            reposCall.enqueue(new Callback<List<GithubRepository>>() {
                @Override
                public void onResponse(Call<List<GithubRepository>> call, Response<List<GithubRepository>> response) {
                    try {
                        CVM.removeProgressBarFragment(ContributorDetailsActivity.this, R.id.ll_repositories_content_contributor_details);
                        if (response.isSuccessful() && response.body().size() > 0) {
                            RecyclerView listRecyclerView = new RecyclerView(ContributorDetailsActivity.this);
                            List<GithubRepository> repositories = response.body();

                            int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
                            GithubRepositoryListAdapter adapter = new GithubRepositoryListAdapter(ContributorDetailsActivity.this, repositories, screenWidth < 1600);

                            listRecyclerView.setLayoutManager(screenWidth < 1600 ? new LinearLayoutManager(ContributorDetailsActivity.this) : new GridLayoutManager(ContributorDetailsActivity.this, 2));
                            listRecyclerView.setAdapter(adapter);

                            LinearLayout contentLinearLayout = findViewById(R.id.ll_repositories_content_contributor_details);
                            contentLinearLayout.removeAllViews();

                            contentLinearLayout.addView(listRecyclerView);
                        } else {
                            TextView noRepoFoundTextView = new TextView(ContributorDetailsActivity.this);
                            String noRepoText = "No repositories by the User : " + contributor.getLogin();
                            noRepoFoundTextView.setText(noRepoText);

                            LinearLayout contentLinearLayout = findViewById(R.id.ll_repositories_content_contributor_details);
                            contentLinearLayout.removeAllViews();

                            contentLinearLayout.addView(noRepoFoundTextView);
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "Error setting up list " + e.toString());
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<GithubRepository>> call, Throwable t) {
                    Log.d(TAG, "Error fetching repositories " + t.toString());
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Log.d(TAG, "Error getting the contributor " + e.toString());
            e.printStackTrace();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
