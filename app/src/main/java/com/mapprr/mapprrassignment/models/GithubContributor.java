package com.mapprr.mapprrassignment.models;

import java.io.Serializable;

public class GithubContributor implements Serializable{

    private String login;
    private int id;
    private String node_id;
    private String avatar_url;
    private String repos_url;

    public String getLogin() {
        return login;
    }

    public int getId() {
        return id;
    }

    public String getNode_id() {
        return node_id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getRepos_url() {
        return repos_url;
    }
}
