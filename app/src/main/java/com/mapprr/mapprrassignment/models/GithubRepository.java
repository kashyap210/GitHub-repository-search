package com.mapprr.mapprrassignment.models;

import java.io.Serializable;

public class GithubRepository implements Serializable{

    private String full_name;
    private String name;
    private RepositoryOwner owner;
    private int stargazers_count;
    private int watchers_count;
    private int forks_count;
    private String html_url;
    private String description;
    private String contributors_url;

    public String getFull_name() {
        return full_name;
    }

    public String getName() {
        return name;
    }

    public RepositoryOwner getOwner() {
        return owner;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public int getWatchers_count() {
        return watchers_count;
    }

    public int getForks_count() {
        return forks_count;
    }

    public String getHtml_url() {
        return html_url;
    }

    public String getDescription() {
        return description;
    }

    public String getContributors_url() {
        return contributors_url;
    }
}
