package com.mapprr.mapprrassignment.models;

import java.io.Serializable;
import java.util.List;

public class GitHubRepositoriesListResponse implements Serializable{

    private int total_count;
    private boolean incomplete_results;
    private List<GithubRepository> items;

    public int getTotal_count() {
        return total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public List<GithubRepository> getItems() {
        return items;
    }
}
