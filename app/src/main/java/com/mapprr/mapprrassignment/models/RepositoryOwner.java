package com.mapprr.mapprrassignment.models;

import java.io.Serializable;

public class RepositoryOwner implements Serializable{

    private String avatar_url;

    public String getAvatar_url() {
        return avatar_url;
    }
}
