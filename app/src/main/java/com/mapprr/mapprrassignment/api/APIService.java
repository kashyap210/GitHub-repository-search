package com.mapprr.mapprrassignment.api;

import com.mapprr.mapprrassignment.models.GitHubRepositoriesListResponse;
import com.mapprr.mapprrassignment.models.GithubContributor;
import com.mapprr.mapprrassignment.models.GithubRepository;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface APIService {

    @GET("search/repositories")
    Call<GitHubRepositoriesListResponse> getListOfGithubRepos(@QueryMap Map<String, String> searchQuery);

    @GET("repos/{repository}/contributors")
    Call<List<GithubContributor>> getListOfContributors(@Path(value = "repository", encoded = true) String repository);

    @GET("users/{user_login}/repos")
    Call<List<GithubRepository>> getListByContributor(@Path(value = "user_login", encoded = true) String login);
}
