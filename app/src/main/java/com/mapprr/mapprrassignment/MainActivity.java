package com.mapprr.mapprrassignment;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.jakewharton.rxbinding2.widget.RxSearchView;
import com.mapprr.mapprrassignment.api.APIAdapter;
import com.mapprr.mapprrassignment.extras.CVM;
import com.mapprr.mapprrassignment.fragments.FiltersFragment;
import com.mapprr.mapprrassignment.fragments.RepositoryListFragment;
import com.mapprr.mapprrassignment.models.GitHubRepositoriesListResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements FiltersFragment.OnFiltersAppliedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private SearchView searchView;
    private boolean isProgressBarVisible = false;
    Call<GitHubRepositoriesListResponse> gitHubRepoListCall;
    private Disposable searchDisposable;
    FloatingActionButton filtersActionButton;
    LinearLayout filtersLinearLayout;
    private boolean areFiltersApplied, isDescendingSelected = false;
    private String filterSortByValue, mostRecentSearchString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setting Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_activity_main);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        //Initial Call for the List
        loadList("android");

        //Filters Icon
        filtersActionButton = findViewById(R.id.fab_activity_main);
        filtersActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFilters();
                filtersActionButton.setVisibility(View.GONE);
                filtersLinearLayout.setVisibility(View.VISIBLE);
            }
        });

        filtersLinearLayout = findViewById(R.id.ll_filter_activity_main);
    }

    private void showFilters() {
        try {
            FiltersFragment filtersFragment = (FiltersFragment) FiltersFragment.newInstance();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
            fragmentTransaction
                    .replace(R.id.ll_filter_activity_main, filtersFragment, "filter_fragment")
                    .commitAllowingStateLoss();
        } catch (Exception e) {
            Log.d(TAG, "Loading filters fragment Err " + e.toString());
            e.printStackTrace();
        }
    }

    private void loadList(String searchCriteria) {
        try {
            //Loading ProgressBar
            if (!isProgressBarVisible) {
                CVM.loadProgressBar(MainActivity.this, R.id.ll_content_main_activity_main);
                isProgressBarVisible = true;
            }

            mostRecentSearchString = searchCriteria;

            Map<String, String> initialStringMap = new HashMap<>();
            initialStringMap.put("q", searchCriteria);
            initialStringMap.put("per_page", "10");
            if (areFiltersApplied) {
                initialStringMap.put("sort", filterSortByValue);
                initialStringMap.put("order", isDescendingSelected ? "desc" : "asc");
            }
            if (gitHubRepoListCall != null) {
                gitHubRepoListCall.cancel();
            }
            gitHubRepoListCall = APIAdapter.getApiService().getListOfGithubRepos(initialStringMap);
            gitHubRepoListCall.enqueue(new Callback<GitHubRepositoriesListResponse>() {
                @Override
                public void onResponse(Call<GitHubRepositoriesListResponse> call, Response<GitHubRepositoriesListResponse> response) {
                    CVM.removeProgressBarFragment(MainActivity.this, R.id.ll_content_main_activity_main);
                    isProgressBarVisible = false;
                    if (response.isSuccessful()) {
                        try {
                            if (response.body().getTotal_count() > 0) {
                                RepositoryListFragment repositoriesListFragment;
                                repositoriesListFragment = (RepositoryListFragment) getSupportFragmentManager()
                                        .findFragmentByTag("list_frag");
                                if (repositoriesListFragment == null) {
                                    repositoriesListFragment = (RepositoryListFragment) RepositoryListFragment.newInstance(response.body().getItems());
                                    Log.d(TAG, "created new frag for " + searchCriteria);
                                }
                                getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.ll_content_main_activity_main, repositoriesListFragment, "list_frag")
                                        .commit();

                                repositoriesListFragment.updateList(response.body().getItems());
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "Error processing the results " + e.toString());
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GitHubRepositoriesListResponse> call, Throwable t) {
                    Log.d(TAG, "Error receiving the repo list " + t.toString());
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            Log.d(TAG, "Error loading list for " + searchCriteria + " " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.options_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_search);

        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);

        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);

        //SearchView setup
        searchDisposable =
                RxSearchView
                        .queryTextChanges(searchView)
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            if (searchView.getQuery().toString().length() > 0) {
                                loadList(searchView.getQuery().toString());
                            } else {
                                loadList("android");
                            }
                        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        searchDisposable.dispose();
    }

    @Override
    public void filtersApplied() {
        filtersActionButton.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                filtersLinearLayout.setVisibility(View.GONE);
            }
        }, 300);
    }

    @Override
    public void filtersApplied(boolean isDescSelected, String sortByValue) {
        try {
            areFiltersApplied = true;
            isDescendingSelected = isDescSelected;
            filterSortByValue = sortByValue;
            loadList(mostRecentSearchString);
        } catch (Exception e) {
            Log.d(TAG, "Error applying filters " + e.toString());
            e.printStackTrace();
        }
    }
}
