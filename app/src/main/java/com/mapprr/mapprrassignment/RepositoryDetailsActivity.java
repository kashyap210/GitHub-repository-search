package com.mapprr.mapprrassignment;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mapprr.mapprrassignment.adapters.ContributorRecyclerAapter;
import com.mapprr.mapprrassignment.api.APIAdapter;
import com.mapprr.mapprrassignment.extras.CVM;
import com.mapprr.mapprrassignment.models.GithubContributor;
import com.mapprr.mapprrassignment.models.GithubRepository;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoryDetailsActivity extends AppCompatActivity {

    private static final String TAG = RepositoryDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository_details);
        Toolbar toolbar = findViewById(R.id.toolbar);

        //Get Details
        GithubRepository repository = (GithubRepository) getIntent().getSerializableExtra("repo_details");
        if (repository != null) {
            try {
                ImageView repoAvatarImageView = findViewById(R.id.iv_repo_avatar_activity_repository_details);
                Picasso
                        .with(RepositoryDetailsActivity.this)
                        .load(repository.getOwner().getAvatar_url())
                        .into(repoAvatarImageView);

                toolbar.setTitle(repository.getName());
                setSupportActionBar(toolbar);

                TextView descriptionTextView = findViewById(R.id.tv_description_content_repo_details);
                descriptionTextView.setText(repository.getDescription());

                TextView projectLinkTextView = findViewById(R.id.tv_project_link_content_repo_details);
                SpannableString content = new SpannableString(repository.getHtml_url());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                projectLinkTextView.setText(content);
                projectLinkTextView.setOnClickListener(view -> {
                    try {
                        Intent webViewIntent = new Intent(RepositoryDetailsActivity.this, WebViewActivity.class);
                        webViewIntent.putExtra("url", repository.getHtml_url());
                        startActivity(webViewIntent);
                    }catch (Exception e){
                        Log.d(TAG, "Opening WebView Activity Err " + e.toString());
                        e.printStackTrace();
                    }
                });

                //Fetching Contributors
                CVM.loadProgressBar(RepositoryDetailsActivity.this, R.id.ll_contributors_content_repo_details);
                Call<List<GithubContributor>> contributorsListCall = APIAdapter.getApiService().getListOfContributors(repository.getFull_name());
                contributorsListCall.enqueue(new Callback<List<GithubContributor>>() {
                    @Override
                    public void onResponse(Call<List<GithubContributor>> call, Response<List<GithubContributor>> response) {
                        if (response.isSuccessful() && response.body().size() > 0) {
                            //Setup RecyclerView
                            RecyclerView contributorsRecyclerView = new RecyclerView(RepositoryDetailsActivity.this);

                            int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
                            contributorsRecyclerView.setLayoutManager(new GridLayoutManager(RepositoryDetailsActivity.this, screenWidth / 300));

                            ContributorRecyclerAapter adapter = new ContributorRecyclerAapter(RepositoryDetailsActivity.this, response.body());
                            contributorsRecyclerView.setAdapter(adapter);

                            CVM.removeProgressBarFragment(RepositoryDetailsActivity.this, R.id.ll_contributors_content_repo_details);
                            LinearLayout contentLinearLayout = findViewById(R.id.ll_contributors_content_repo_details);
                            contentLinearLayout.removeAllViews();
                            contentLinearLayout.addView(contributorsRecyclerView);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<GithubContributor>> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                Log.d(TAG, "Error setting repo details " + e.toString());
                e.printStackTrace();
            }
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
