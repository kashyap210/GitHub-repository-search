package com.mapprr.mapprrassignment.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mapprr.mapprrassignment.R;

public class FiltersFragment extends Fragment {

    private static final String TAG = FiltersFragment.class.getSimpleName();
    private boolean isDescendingSelected = true;
    private boolean isSortBySelected = false;
    private String sortByValue = "";
    LinearLayout starsLinearLayout, forksLinearLayout, updatedLinearLayout;

    public static Fragment newInstance() {
        return new FiltersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filters, container, false);

        LinearLayout ascendingLinearLayout = view.findViewById(R.id.ll_order_by_ascending_fragment_filters);
        LinearLayout descendingLinearLayout = view.findViewById(R.id.ll_order_by_desc_fragment_filters);
        starsLinearLayout = view.findViewById(R.id.ll_sort_by_stars_fragment_filters);
        forksLinearLayout = view.findViewById(R.id.ll_sort_by_forks_fragment_filters);
        updatedLinearLayout = view.findViewById(R.id.ll_sort_by_updated_fragment_filters);

        //Ascending Click
        ascendingLinearLayout.setOnClickListener(view13 -> {
            try {
                isDescendingSelected = false;
                ascendingLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_selected_filter));
                descendingLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_unselected_filter));
            } catch (Exception e) {
                Log.d(TAG, "Error selecting ascending " + e.toString());
                e.printStackTrace();
            }
        });

        //Descending Click
        descendingLinearLayout.setOnClickListener(view1 -> {
            try {
                isDescendingSelected = true;
                ascendingLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_unselected_filter));
                descendingLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_selected_filter));
            } catch (Exception e) {
                Log.d(TAG, "Error selecting descending " + e.toString());
                e.printStackTrace();
            }
        });

        //Stars Click
        starsLinearLayout.setOnClickListener(view1 -> {
            if (!isSortBySelected) {
                isSortBySelected = true;
            } else {
                unSelectPreviousValue();
            }
            starsLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_selected_filter));
            sortByValue = "stars";
        });

        //Forks Click
        forksLinearLayout.setOnClickListener(view1 -> {
            if (!isSortBySelected) {
                isSortBySelected = true;
            } else {
                unSelectPreviousValue();
            }
            forksLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_selected_filter));
            sortByValue = "forks";
        });

        //Updated Click
        updatedLinearLayout.setOnClickListener(view1 -> {
            if (!isSortBySelected) {
                isSortBySelected = true;
            } else {
                unSelectPreviousValue();
            }
            updatedLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_selected_filter));
            sortByValue = "updated";
        });

        //Apply Filters
        view.findViewById(R.id.iv_submit_fragment_filters).setOnClickListener(view1 -> {
            try {
                OnFiltersAppliedListener onFiltersAppliedListener = (OnFiltersAppliedListener) getActivity();
                if (isSortBySelected) {
                    onFiltersAppliedListener.filtersApplied(isDescendingSelected, sortByValue);
                }
                onFiltersAppliedListener.filtersApplied();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
                fragmentTransaction.remove(FiltersFragment.this).commit();
            } catch (Exception e) {
                Log.d(TAG, "Apply filters button click Err " + e.toString());
                e.printStackTrace();
            }
        });

        //Cancel Button
        view.findViewById(R.id.iv_cancel_fragment_filters).setOnClickListener(view12 -> {
            try {
                OnFiltersAppliedListener onFiltersAppliedListener = (OnFiltersAppliedListener) getActivity();
                onFiltersAppliedListener.filtersApplied();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_bottom, R.anim.slide_out_bottom);
                Fragment filterFragment = getActivity().getSupportFragmentManager().findFragmentByTag("filter_fragment");
                fragmentTransaction.remove(filterFragment).commitNowAllowingStateLoss();
            } catch (Exception e) {
                Log.d(TAG, "Finishing filters fragment Err " + e.toString());
                e.printStackTrace();
            }
        });

        return view;
    }

    private void unSelectPreviousValue() {
        switch (sortByValue) {
            case "stars":
                starsLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_unselected_filter));
                break;

            case "forks":
                forksLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_unselected_filter));
                break;

            case "updated":
                updatedLinearLayout.setBackground(getResources().getDrawable(R.drawable.style_unselected_filter));
                break;
        }
    }

    public interface OnFiltersAppliedListener {
        void filtersApplied();

        void filtersApplied(boolean isDescendingSelected, String sortByValue);
    }
}
