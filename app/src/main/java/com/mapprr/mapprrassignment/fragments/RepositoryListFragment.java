package com.mapprr.mapprrassignment.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapprr.mapprrassignment.R;
import com.mapprr.mapprrassignment.adapters.GithubRepositoryListAdapter;
import com.mapprr.mapprrassignment.models.GithubRepository;

import java.io.Serializable;
import java.util.List;

public class RepositoryListFragment extends Fragment {

    private static final String TAG = RepositoryListFragment.class.getSimpleName();
    GithubRepositoryListAdapter adapter;

    public static Fragment newInstance(List<GithubRepository> items) {
        Fragment fragment = new RepositoryListFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("items", (Serializable) items);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repository_list, container, false);

        RecyclerView listRecyclerView = view.findViewById(R.id.rv_fragment_repository_list);
        List<GithubRepository> repositories = (List<GithubRepository>) getArguments().getSerializable("items");

        int screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        adapter = new GithubRepositoryListAdapter(getContext(), repositories, screenWidth < 1600);

        listRecyclerView.setLayoutManager(screenWidth < 1600 ? new LinearLayoutManager(getContext()) : new GridLayoutManager(getContext(), 2));
        listRecyclerView.setAdapter(adapter);

        return view;
    }

    public void updateList(List<GithubRepository> items) {
        try {
            if (adapter != null) {
                adapter.setData(items);
            }
        } catch (Exception e) {
            Log.d(TAG, "Error updating list " + e.toString());
            e.printStackTrace();
        }
    }
}
